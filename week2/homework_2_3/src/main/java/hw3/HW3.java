package hw3;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author zdenda
 */
public class HW3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try (MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost"))) {
            final MongoDatabase database = mongoClient.getDatabase("students");
            final MongoCollection<Document> grades = database.getCollection("grades");

            List<Document> sort = grades.find(new Document("type", "homework")).sort(new Document("student_id", 1)
                    .append("score", -1)).into(new ArrayList<Document>());

            Integer sId = null;
            ObjectId lastDocumentId = null;
            for (Document document : sort) {
                Integer student = document.getInteger("student_id");
                if (lastDocumentId != null && !student.equals(sId)) {
                    grades.deleteOne(new Document("_id", lastDocumentId));
                }
                lastDocumentId = document.getObjectId("_id");
                sId = student;
            }

            //remove last element
            grades.deleteOne(new Document("_id", sort.get(sort.size() - 1).getObjectId("_id")));

        }

    }

}
